# mrapis

This is used to query various Crossref APIs. 

get_events: query the Event Data API
get_items: query the item graph kernel APIs (currently items of relationships)
get_relationships: query the realtionships endpoint

to query any other JSON API, use:
query_api