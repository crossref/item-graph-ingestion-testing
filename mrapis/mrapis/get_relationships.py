
from .query_api import query_json_api

class get_relationships():

    def __init__(self):

        self.query_url = 'https://manifold-api.production.crossref.org/v1/relationships'
        self.results_field = 'relationships'

    def make_query(self, params = {}):

        js, status = query_json_api(self.query_url, params)
        if status != 200:
            print('something when wrong with the relationships endpoint', status)
            return []

        rels = js[self.results_field]
        return rels

if __name__=="__main__":

    igr = get_relationships()
    js = igr.query_rels({'uri': 'https://doi.org/10.1111/j.1467-8330.2010.00827.x', 'rows':5})

    print(len(js))