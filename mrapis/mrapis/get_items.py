'''
Get items from the Crossref item tree

'''

import requests
from .query_api import query_json_api, multipage_query

class get_items:

    def __init__(self):
        """
        That said, property-statements-with-provenance:
            cursor
            rows
            subject
            assertedBy

            relationship-statements-with-provenance:
            cursor
            rows
            subject
            object
            relationshipType
            assertedBy
        
        """

        self.relation_endpoint = "https://manifold-api.production.crossref.org/v2/relationship-statements-with-provenance"
        self.property_endpoint = "https://manifold-api.production.crossref.org/v2/property-statements-with-provenance"
        self.item_tree_endpoint = "https://manifold-api.production.crossref.org/v2/item-tree/"

    def use_staging(self):

        self.relation_endpoint = "https://manifold-api.staging.crossref.org/v2/relationship-statements-with-provenance"
        self.property_endpoint = "https://manifold-api.staging.crossref.org/v2/property-statements-with-provenance"
        self.item_tree_endpoint = "https://manifold-api.staging.crossref.org/v2/item-tree/"
    
    def make_property_query(self, params):
        ''' Query the property endpoint '''

        json_results, status = query_json_api(self.property_endpoint, params)

        return json_results, status 

    def make_relationship_query(self, params):
        ''' Query the relationships endpoint '''

        json_results, status = query_json_api(self.relation_endpoint, params)

        return json_results, status

    def make_item_tree_query(self, id):
        ''' Get an item tree for an identifier '''

        json_results, status = query_json_api(self.item_tree_endpoint + id)

        return json_results, status


    def multipage_relationship_query(self, params, max_pages = 50, rows = 100):
        ''' Query with cursors '''

        max_attempts = 5
        relationships = []

        params['rows'] = rows

        for x in range(max_pages):
            print('page', x)
            
            json_results, status = self.json_api_query(self.relation_endpoint, params)

            # handle failed queries
            if (status>=400) & (status < 500):
                print('check input parameters, query failed:', status)
                break
            elif status != 200:
                failed_attempts += 1

                # give up, we've failed too many times
                if failed_attempts == max_attempts:
                    print('query could not be completed, failed too many times. Latest status:', status)
                    break        

                continue

            failed_attempts = 0

            # get the next cursor
            params['cursor'] = json_results['message']['next-cursor']

            # add the results to output
            relationships += json_results['message']['relationship-statements-with-provenance']

            # stop conditions
            if len(json_results['message']['relationship-statements-with-provenance']) != rows:
                break

        return relationships



if __name__=="__main__":

    import pprint
    import json

    itr = get_items()

    js = itr.relationship_query({'object': 'https://doi.org/10.1093/ref:odnb/8516', 'rows':10})

    with open('rel_test.json', 'w') as f:
        json.dump(js, f, indent=2)

    pprint.pprint(js)