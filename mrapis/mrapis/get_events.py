'''
A module for querying Crossref Event Data
'''

import requests
from .query_api import query_json_api, multipage_query

class get_events:

    def __init__(self):

        self.event_data_api_url = 'https://api.eventdata.crossref.org/v1/events'


    def make_query(self, query_parameters:dict)->tuple:
        ''' 
        Get events from the Crossref Event Data API.

        Parameters:
        -----------
        queryParameters: dict
            values are api query fields, values are query parameters.
            See https://www.eventdata.crossref.org/guide/service/query-api/ for allowed fields.


        Returns:
        --------
        dict:
            json formatted events
        int:
            html status from the API query (200 for a successful query)
        
        '''
        
        # use function in query_api
        js, status = query_json_api(self.event_data_api_url, query_parameters)

        return js, status

    def multipage_query(self, query_parameters:dict, max_pages = 50, rows = 500) -> list:
        '''
        Use cursors to get multiple pages of a query of Crossref Event Data.

        Parameters:
        -----------
        queryParameters: dict
            values are api query fields, values are query parameters.
            See https://www.eventdata.crossref.org/guide/service/query-api/ for allowed fields.

        maxPages: int
            the maximum number of pages to return. 

        rows : int
            how many rows to return from each query

        Returns:
        --------
        list:
            list json formatted events, without headers      
        
        '''

        # use function in query_api
        events = multipage_query(self.event_data_api_url, 'events', query_parameters, max_pages = max_pages, rows = rows, max_attempts = 5)

        return events

