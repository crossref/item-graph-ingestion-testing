'''
Simple wrapper for requests to query a generic API
'''

import requests

def find_fields(d:dict, field):
    ''' Sometimes we're not quite sure where a field is. Here we 
    try and find it in the top level field, otherwise look under message'''

    if field in d:
        x = d[field]

        return x
    elif 'message' in d:
        dm = d['message']
        if field in dm:
            x = d['message'][field]

            return x
    
    raise KeyError("'" + str(field) + "'" + " and 'message' not found in dictionary")



def query_json_api(url, parameters = {}):
    ''' Query a JSON endpoint with some parameters'''

    r = requests.get(url, params = parameters)

    if r.status_code != 200:
        print('query failed', r.status_code)
        print(r.text)
        return {}, r.status_code

    js = r.json()

    return js, r.status_code


def multipage_query(url:str, results_name:str, parameters = {}, max_pages = 10, rows = 10, max_attempts = 5):
    """
    Query using cursors. Note that a 400 error will cause an immediate stop. Several retries
    are made for other failure modes. You might require a cursor to be defined in your parameters.
    Note that if you include rows in your parameters it will be overwritten. 

    The next cursor is assumed to be in json['message']['next-cursor']

    url: str
        The url of the API you want to query

    params: dict
        query parameters
    results_name:
        name of the field (top level or under 'message') containing the results
    max_pages: int
        the maximum number of pages to return
    rows: int
        how many rows (results) to include in each page
    max_attempts: int
        if the query fails, how often should we retry until we give up?

    returns:
    --------
    list:
        list of items found from each page
        

    """

    results_list = [] # output

    params = parameters.copy()
    params['rows'] = rows # add rows to the parameters

    for x in range(max_pages):
        print('page', x)
        
        json_results, status = query_json_api(url, params)

        # handle failed queries
        if (status>=400) & (status < 500):
            print('check input parameters, query failed:', status)
            break
        elif status != 200:
            failed_attempts += 1

            # give up, we've failed too many times
            if failed_attempts == max_attempts:
                print('query could not be completed, failed too many times. Latest status:', status)
                break        

            continue

        failed_attempts = 0

        # get the next cursor
        try:
            params['cursor'] = find_fields(json_results, 'next-cursor')
        except KeyError:
            print('Finding cursor failed. Check if you need to specify a cursor in the API parameters.')
            break

        # add the results to output
        new_results = find_fields(json_results, results_name)
        results_list += new_results

        # stop conditions
        if len(new_results) != rows:
            break

    return results_list

def content_negotiation_query(doi, headers = {}, recipe = None):
    ''' Get data back in some format. This is basically a wrapper for a requests
     query with headers. You can use a recipe that gives some predefined headers.
      
     Using a recipe overwrites the headers passed to the function.
       
         '''

    works_api_url = 'https://api.crossref.org/v1/works/'

    # some predefined headers
    if recipe == 'bibtex1':
        headers = {"Accept": "application/x-bibtex"}
    elif recipe == 'bibtex2': 
        headers = {"Accept": "text/bibliography; style=bibtex"}

    url = works_api_url + doi + '/transform'
    r = requests.get(url, headers=headers)

    return r.text, r.status_code


if __name__=="__main__":
    
    doi = "10.1145/3448016.3452841"

    b, status = content_negotiation_query(doi, recipe='bibtex2')

    print(status)
    print(b)