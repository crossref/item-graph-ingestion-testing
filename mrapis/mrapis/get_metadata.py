from .query_api import query_json_api, multipage_query

class get_metadata():

    def __init__(self):

        self.works_url = 'https://api.crossref.org/v1/works/'

    def make_query(self, params: dict, filter_params = {}) -> tuple:
        ''' Query the Crossref REST API works endpoint. Returns the full results.

        Filters have a weird format, they're converted from a dictionary to the needed format here. 
        
        Returns:
        (dict, int):
            results in json format and the status

        '''

        # sort out the filters
        fl = ''

        ## add each filter
        for field in filter_params:
            fl += field + ':' + str(filter_params[field]) + ','

        ## add filter to the query parameters
        if len(fl)>0:
            fl = fl[:-1]
            params['filter'] = fl


        # make the query
        js, status = query_json_api(self.works_url, params)
        if status != 200:
            print('something when wrong with the works endpoint', status)
            return {}, status

        return js, status
    
    def make_multipage_query(self, params:dict, max_pages:int, rows_per_page:int, filter_params={}, max_attempts=5):

        query_params = params.copy()
        query_params['cursor'] = '*'
        query_params['rows'] = rows_per_page

        # sort out the filters
        fl = ''

        ## add each filter
        for field in filter_params:
            fl += field + ':' + str(filter_params[field]) + ','

        ## add filter to the query parameters
        if len(fl)>0:
            fl = fl[:-1]
            query_params['filter'] = fl

        items = multipage_query(self.works_url, 'items', query_params, max_pages=max_pages, rows = rows_per_page, max_attempts=max_attempts)

        return items
   