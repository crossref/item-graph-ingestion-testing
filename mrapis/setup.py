#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 29 08:43:35 2020

@author: martynrittman
"""

from setuptools import setup

setup(name='mrapis',
      version='0.1',
      description='Run queries on JSON APIs',
      url='https://api.crossref.org',
      author='Martyn Rittman',
      author_email='mrittman@crossref.org',
      license='MIT',
      packages=['mrapis'],
      zip_safe=False)