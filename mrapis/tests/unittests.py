import unittest
import mrapis
from mrapis.query_api import find_fields
import json

class TestStringMethods(unittest.TestCase):

    #@unittest.skip("skipping general json query")
    def test_query_json_api(self):
        ''' test query_api.query_json_api '''

        url = 'https://api.crossref.org/v1/works/10.3390/atoms1010001'
        js, status = mrapis.query_json_api(url)

        print('html response:', status)

    @unittest.skip("skipping multipage query")
    def test_multipage_query(self):
        ''' test a simple multi-page query '''

        url = 'https://api.crossref.org/v1/works'
        js_list = mrapis.multipage_query(url, 'items', parameters={'cursor':'*'}, max_pages=3, rows=5)
        print(len(js_list), 'items found, 15 expected')

    @unittest.skip("skip testing multiple query with no cursor")
    def test_multipage_query_no_cursor(self):
        ''' test a simple multi-page query '''

        url = 'https://api.crossref.org/v1/works'
        js_list = mrapis.multipage_query(url, 'items', max_pages=3, rows=5)
        print(len(js_list), 'items found, 0 expected')

    def test_find_fields(self):
        d = {'a':['a1'], 'b':['b1'], 'c':['b1']}
        try:
            dx = find_fields(d, 'x')
            print(dx)
        except KeyError:
            print("didn't find Mr X, as expected")

        df =find_fields(d, 'b')
        print(df, df==['b1'])

        d['message'] = {'x': ['x1']}
        dx = find_fields(d, 'x')
        print(dx, dx==['x1'])

    #@unittest.skip("skipping content negotiation test")
    def test_content_negotiation_query(self):
        doi = "10.1145/3448016.3452841"

        b, status = mrapis.content_negotiation_query(doi, recipe='bibtex1')

        print(status)
        print(b)

    def test_get_metadata(self):
        params = {
            'mailto': 'mrittman@crossref.org'
        }

        fl = {
            'from-pub-date': '2023-01-01',
            'until-pub-date': '2023-01-01'
        }

        query = mrapis.get_metadata()
        js, status = query.make_query(params, fl)

        print(js['message']['total-results'], 'records from works endpoint test')

    @unittest.skip("skipping metadata multipage query test")
    def test_get_metadata_multipage_query(self):

        gm = mrapis.get_metadata()

        params = {}
        works = gm.make_multipage_query(params, 2, 10)

        with open('works multipage test.json', 'w') as f:
            json.dump(works, f, indent=2)

        print("See 'works multipage test.json' for multiple works test output")

if __name__ == '__main__':
    
    unittest.main() 