'''
Test ingestion of items to the item graph by comparing field values in the REST API with 
those in the item graph
'''

import mrapis
import json
import os

def string_converter(s_in):
    ''' change weird characters in a string '''
    chars = ['/', '.', '\\']
    s = s_in

    for char in chars:
        # superfluous limit of retries just in case...
        for x in range(1000):
            while (char) in s:
                s = s.replace(char, '-')

    return s

class ingestion_tester():

    def __init__(self):

        # API locations
        self.rest_api_url = 'https://api.crossref.org/v1/works'
        self.item_graph_url = 'https://manifold-api.staging.crossref.org/v2/item-tree'

        # file locations
        self.rest_api_json_folder = 'test/'
        self.item_graph_json_folder = 'test/'


    def get_json_from_api(self, identifier:str, mode:str, filename = '') -> dict:
        ''' Retrieve a metadata record from the production Crossref REST API works endpoint.

        Parameters: 
        -----------
        identifer: str
            A Crossref DOI. 

        mode:
            can be 'cr_metadata' or 'item_tree', deteremins which API to query

        filename:
            name of a filename to look for. If it doesn't exist, the output is saved to this filename. 
            use '' to avoid looking or saving to file.

        Returns:
        --------
        dict:
            JSON of metadata record.

        int:
            status of API query response
        '''

        ### is there a previous version?
        if (len(filename) > 0)&(os.path.exists(filename)):
            # load from file
            with open(filename, 'r') as f:
                js = json.load(f)
        else:
            # query the API
            if mode == 'cr_metadata':
                js, status = mrapis.query_json_api(self.rest_api_url + '/' + identifier)
            elif mode == 'item_tree':
                js, status = mrapis.query_json_api(self.item_graph_url + '/' + identifier)
            else:
                print('wrong mode selected!')
                return {}

            # save the json to file
            if (len(filename)>0)&(status == 200):
                with open(filename, 'w') as f:
                    json.dump(js, f, indent=2)

        # record the metadata for later use
        if mode == 'cr_metadata':
            self.rest_api_json = js
        elif mode == 'item_tree':
            self.item_tree_json = js
        else:
            print('wrong mode selected!')
            return {}

        return js
    
    def load_fields(self, field_filename:str) -> dict:
        ''' Load the list of fields we'll be comparing. Uses JSON format with a dictionary:

            Keys are field titles. Values are dict with keys 'rest api' and 'item graph'
            Values of each are lists of fields where the desired metadata should be located. 

            e.g. 

            {
                title: {
                    'rest api': ['title'],
                    'item graph':['title']
                }
            }

        Parameters:
        -----------
        field_filename: str
            name of the file containing data in the correct format. 

        Returns:
        --------
        the loaded json as a dictionary
           
        '''

        with open(field_filename, 'r') as f:
            fields = json.load(f)

        self.field_mappings = fields
        return fields

    def compare_fields(self, verbose = False) -> dict:
        ''' Compare self.rest_api_json and self.item_graph_json
        
        Needs self.field_mappings to be set, use self.load_fields to do that.

        Output:
        -------
        dict: 
            keys are the same as field_mappings. Value are booleans indicating success or failure.
        
        '''

        # output
        field_test_output = {}

        if verbose:
            values = {}

        # check eeach field name
        for field in self.field_mappings:
            fm = self.field_mappings[field]
            # try and get data from the rest api and item tree jsons
            rest_api_value = self.get_field(self.rest_api_json, fm['rest api'])
            item_tree_value = self.get_field(self.item_tree_json, fm['item tree'])

            if verbose:
                value = {
                    'rest api': rest_api_value,
                    'item tree': item_tree_value
                }
                values[field] = value

            # record output: were the two the same?
            if not(rest_api_value) or not(item_tree_value):
                # case where at least one value wasn't found
                field_test_output[field] = False
            else:
                # case where both values were found

                ## check for format information
                if 'format' in fm:
                    [rest_api_value, item_tree_value] = self.format_values([rest_api_value, item_tree_value], self.field_mappings[field]['format'])
                    
                field_test_output[field] = rest_api_value == item_tree_value           

        if verbose:
            verbose_output = {
                'test result': field_test_output,
                'values': values
            }

            return verbose_output

        return field_test_output
    

    def format_values(self, vals, format):
        ''' reformat some values in case there's a mismatch between expected types
         
        format can be, e.g. ["int", x] where the output is strings of length x

            '''

        if format[0] == "int":
            # convert integers to strings of the same length
            output_vals = [str(v).zfill(format[1]) for v in vals]

        else:
            output_vals = vals
        
        return output_vals
                


    def get_field(self, d:dict, field_path:list):
        ''' Find a value in a json dictionary by following a set of keys and indexes
        
        Parameters:
        -----------
        d: dict
            any dictionary representing JSON data

        field_path: list
            where to find the value you're looking for

        Returns:
        --------
        any: 
            some value, could be of any type.
            Returns None if there is a problem finding the value sought.
        
         '''

        value = d.copy()
        for f in field_path:
            try:
                value = value[f]
            except KeyError:
                value = None
                break
            except IndexError:
                value = None
                break
            except TypeError:
                break
        
        return value
    
    def check_identifier(self, identifier: str, field_names_filename: str, verbose=False) -> dict:
        ''' Put in an identifier, retrieve the JSON records for it from the REST API and
        item graph, then see if the expected fields match up. 

        Parameters:
        -----------
        identifier: str
            an identifier, probably a DOI.

        field_names_filename: str
            the filename of a json doc detailing which fields should match for each output.
            See the function load_fields for a description of the format.

        save_json: bool
            saves the json to file if true

        returns:
        --------
        dict:
            a dictionary with field names as keys and values as boolean stating whether the values
            from each source matched.        
            
        '''

        # load the field names
        self.load_fields(field_names_filename)

        # get data
        
        ## from rest API works endpoint
        rest_api_filename = self.rest_api_json_folder + 'rest_api_' + string_converter(identifier) + '.json'
        self.get_json_from_api(identifier, 'cr_metadata', filename=rest_api_filename)

        ## from item tree endpoint
        item_tree_filename = self.rest_api_json_folder + 'item_tree_' + string_converter(identifier) + '.json'
        self.get_json_from_api(identifier, 'item_tree', filename=item_tree_filename)

        # compare
        field_by_field_comparison = self.compare_fields(verbose=verbose)

        return field_by_field_comparison
    