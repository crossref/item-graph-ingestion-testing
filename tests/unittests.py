import unittest
import ingestion_tester
import json
import os
from ingestion_tester import string_converter

class TestStringMethods(unittest.TestCase):

    def test_get_json_from_api(self):

        it = ingestion_tester.ingestion_tester()

        doi = '10.3390/atoms1010001'
        it.get_json_from_api(doi, 'item_tree')

    def test_get_field(self):

        it = ingestion_tester.ingestion_tester()

        d = {
                'title': 'this is the title',
                'authors': ['author a', 'author b']
            }

        f1 = ['title']
        f2 = ['authors', 0]

        val1 = it.get_field(d, f1)
        res1 = val1 == 'this is the title'

        val2 = it.get_field(d, f2)
        res2 = val2 == 'author a'

        val3 = it.get_field(d, ['unknown field'])
        res3 = val3 == None

        print('item test results :')
        print(res1, val1)
        print(res2, val2)
        print(res3, val3)

    def test_compare_fields(self):

        it = ingestion_tester.ingestion_tester()
        
        it.rest_api_json = {
            'title': 'this is not the title',
            'authors': ['author a2', 'author b2'],
            'abstract': 'text'
        }

        it.item_tree_json = {
            'title': 'this is not the title',
            'authors': ['author a2', 'author b2']
        }

        ra = 'rest api'
        itt = 'item tree'

        it.field_mappings = {
                'title': {
                    ra: ['title'],
                    itt: ['title']
                },
                'first author': {
                    ra: ['authors', 0],
                    itt: ['authors', 0]
                },
                'abstract': {
                    ra: ['abstract'],
                    itt: ['abstract']
                }
            }

        res = it.compare_fields()

        expected_res = {
            'title': True,
            'first author': True,
            'abstract': False
        }
        res_compare = res == expected_res

        print("compare fields results (expect {'title': True, 'first author': True, 'abstract': False}:")
        print(res)
        print(res_compare)

    @unittest.skip("skipping identfier check")
    def test_check_identifier(self):

        x = os.getcwd()
        print(x)

        it = ingestion_tester.ingestion_tester()
        res = it.check_identifier('10.3390/atoms1010001', 'ingestion_tester/test_field_names.json')

        print("check_identifier results (expect {'title': False, 'first author': False, 'abstract': False}):")
        print(res)


    def test_string_converter(self):

        s = "10.2290/abcd\\a;obj.1234"
        s_exp = "10-2290-abcd-a;obj-1234"

        s_out = string_converter(s)

        print('string converter test:')
        print(s_out == s_exp)

    def test_value_formatter(self):

        values = [1, '1']
        format_fail = ["fail", 3]
        format_succeed = ["int", 2]


        it = ingestion_tester.ingestion_tester()
        v_fail = it.format_values(values, format_fail)
        print('value formatting results:')
        print(v_fail, 'expect [1, "1"]')

        v_succeed = it.format_values(values, format_succeed)
        print(v_succeed, 'expected["01", "01"]')

    def test_comparison_with_format(self):
        it = ingestion_tester.ingestion_tester()
        
        it.rest_api_json = {
            'title': 'this is not the title',
            'page': '1',
        }

        it.item_tree_json = {
            'title': 'this is not the title',
            'page': 1
        }

        ra = 'rest api'
        itt = 'item tree'

        it.field_mappings = {
                'title': {
                    ra: ['title'],
                    itt: ['title']
                },
                'first author': {
                    ra: ['authors', 0],
                    itt: ['authors', 0]
                },
                'abstract': {
                    ra: ['abstract'],
                    itt: ['abstract']
                },
                'page': {
                    ra: ['page'],
                    itt: ['page'],
                    'format': ["int", 2]
                }
            }

        res = it.compare_fields()

        expected_res = {
            'title': True,
            'first author': True,
            'abstract': False,
            'page': True
        }
        res_compare = res == expected_res

        print("compare fields results with format (expect {'title': True, 'first author': True, 'abstract': False, 'page': True}:")
        print(res)
        print(res_compare)

if __name__ == '__main__':
    
    unittest.main() 