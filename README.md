# Item Graph ingestion testing

This project contains some tools to make testing items added to the Item Graph less painful. The ingestion_tester package enables easy comparision between outputs from the works endpoint of the Crossref REST API and the item-tree endpoint of the Manifold API. 

For an example of how to use the code and what the output looks like, see check_a_doi.ipynb.